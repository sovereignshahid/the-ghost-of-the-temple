extends AudioStreamPlayer

var violin_stab = preload("res://violin_stab.ogg")
var has_entered = false 
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_trigger_entity_running_body_entered(body):
	if body.name == "PlayerController":
		if has_entered == false:
			stream = violin_stab
			playing = true
