extends Node3D

@onready var animation_player = $"Fast Run/AnimationPlayer"
# Called when the node enters the scene tree for the first time.
func _ready():
	animation_player.current_animation = "Armature|mixamocom|Layer0"


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not animation_player.is_playing():
		queue_free()
